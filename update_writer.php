<!DOCTYPE html>
<html lang="en">

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

       <!-- Fonts-->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Lobster|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">

    <link href="css/style.css" rel="stylesheet">

</head>

<style rel="stylesheet" type="text/css" href="style.css">
    #img_div {
        width: 80%;
        padding: 5px;
        margin: 15px auto;
        border: 1px solid #cbcbcb;
    }
    
    #img_div:after {
        content: "";
        display: block;
        clear: both;
    }
    
    img {
        float: left;
        margin: 5px;
        width: 200px;
        height: 140px;
    }
    
    #bs-link {
        width: 100%;
        margin-top: 11px;
    }
    
    input#name.form-control {
        padding-left: 0px;
    }
    
    input#destination_two.form-control {
        padding-left: 0px;
    }
</style>

</head>

<body id="page-top">
    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="custom-nav">

        <a class="navbar-brand logo-txt" href="http://localhost/savasaachi_admin_panel/dashboard.php">Savasaachi</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

            </ul>

        </div>

    </nav>

    <?php
session_start();
require("connection.php");
$id=$_GET['id'];

if(isset($id))
{

  $readdata= "SELECT * FROM content WHERE id = '$id' "; 
  $queries= mysqli_query($conn,$readdata);
  $queriesarray=mysqli_fetch_array($queries);
  $name=$queriesarray['name'];
  $date=$queriesarray['date'];
  $post_material=$queriesarray['post_material'];
  $tags=$queriesarray['tags'];
  $poster_material=$queriesarray['poster_material'];
  $vision=$queriesarray['vision'];
  $comment=$queriesarray['comment'];

}

if (isset($_POST['updatedata'])) {

  $name=$_POST['name'];
  $date=$_POST['date'];
  $post_material=$_POST['post_material'];
  $tags=$_POST['tags'];
  $poster_material=$_POST['poster_material'];
  $vision=$_POST['vision'];
  $comment=$_POST['comment'];

  $upquery="UPDATE `content` SET  `name`='$name',`date`='$date',`post_material`='$post_material',`tags`='$tags',
  `poster_material`='$poster_material', `vision`='$vision',`comment`='$comment' WHERE id=  '".$id."'"  ;
  $updated=mysqli_query($conn,$upquery);
  if($updated==true)
  {
    ?>
        <script>
            alert("Record updated Successfully");
            window.location.href = "writer.php";
        </script>
        <?php
  }
}
?>

            <!--Content Form -->

            <div class="container form-txt-five">
                <div id="con" class="card">
                    <div class="card-header col-md-12">
                      Update Content
                      <a href="javascript:history.back()" class="btn btn-info btn-sm float-right custom-btn">Back</a>
                    </div>

                        <form action="#" method="POST" enctype="multipart/form-data">
                    <div class="card-body">
                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <div class="form-label-group">

                                            <input type="text" class="form-control" id="name" placeholder="name" name="name" value="<?php echo $name; ?>">
                                            <label for="firstName">Name</label>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">

                                        <div class="form-label-group">
                                            <input type="text" name="date" id="date" class="form-control" placeholder="From Date" value="<?php echo $date; ?>" style="margin-top: 10px;">
                                            <label for="date">Date</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" id="post_material" placeholder="districts" name="post_material" value="<?php echo $post_material; ?>">
                                            <label for="inputPostMaterial">Post Material</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" id="tags" placeholder="Enter division" name="tags" value="<?php echo $tags; ?>">
                                            <label for="inputTags">Tags</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" id="poster_material" placeholder="Enter division" name="poster_material" value="<?php echo $poster_material; ?>">
                                            <label for="inputPosterMaterial">Poster Material</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input class="form-control" rows="5" id="vision" name="vision" value="<?php echo $vision; ?>">
                                            <label for="inputVision">Vision</label>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">
                                <div class="form-label-group">
                                    <input class="form-control" rows="5" id="comment" name="comment" value="<?php echo $comment; ?>">
                                    <label for="inputComment">Comment</label>
                                </div>
                            </div>

                    </div>
                    <div class="card-footer text-right">
                      <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:history.back()" class="btn btn-info custom-btn btn-sm custom-btn">Cancle</a>
                            <button type="submit" class="btn btn-info btn-sm custom-btn" name="updatedata">Submit</button>
                        </div>
                    </div>
                        </form>
                </div>
            </div>

            <!-- Sticky Footer -->
            <footer class="sticky-footer-two form-txt-five">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © Your Website 2018</span>
                    </div>
                </div>
            </footer>





            <!-- Datepicker -->

            <script>
                $(document).ready(function() {
                    $.datepicker.setDefaults({
                        dateFormat: 'dd/mm/yy'
                    });
                    $(function() {
                        $("#date").datepicker();
                    });

                });
            </script>

            <!-- Search -->

            <script>
                $(document).ready(function() {
                    $("#myInput").on("keyup", function() {
                        var value = $(this).val().toLowerCase();
                        $("#myTable a").filter(function() {
                            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                        });
                    });
                });
            </script>

            <!-- Bootstrap core JavaScript-->

            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Page level plugin JavaScript-->
            <script src="vendor/datatables/jquery.dataTables.js"></script>
            <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin.min.js"></script>

            <!-- Demo scripts for this page-->
            <script src="js/demo/datatables-demo.js"></script>

</body>

</html>