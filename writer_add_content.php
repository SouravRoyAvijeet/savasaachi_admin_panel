<?php
  date_default_timezone_set('Asia/Dhaka');
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Add Content</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/style.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

  </head>

  <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">

          <a class="navbar-brand" href="http://localhost/savasaachi_admin_panel/writer.php">Savasaachi</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">

              <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

                <li class="nav-item ">
                  <a class="nav-link" href="content_detail.php">Content</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="add_content_three.php">Write Content</a>
                </li>
             
              </ul>

              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>
             
          </div>

        </nav>

    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Add Content</div>
        <div class="card-body">

          <form action="add_content_system.php" method="POST">

            <div class="form-group">
                <label for="firstName">Name</label>             
                  <div class="form-label-group">
                      <input name="name" type="text" id="firstName" class="form-control" placeholder="Name" required="required" autofocus="autofocus">                      
                  </div>
            </div>

            <div class="form-group">
             <label for="inputDate">Date</label>
              <div class="form-label-group">
                <input name="date" class="dates form-control" placeholder="Date" type="text" id="datepicker" required="required" value="<?php echo  date("d/m/Y") ?> ">
               
              </div>
            </div>

            <div class="form-group">
                <div class="form-label-group">
                    <input name="post_material" type="text" id="inputPostMaterial" class="form-control" placeholder="Post Material" required="required">
                    <label for="inputPostMaterial">Post Material</label>
                </div>
            </div>

            <div class="form-group">
                <div class="form-label-group">
                    <input name="tags" type="text" id="inputTags" class="form-control" placeholder="Tags" required="required">
                    <label for="inputTags">Tags</label>
                </div>
            </div>

            <div class="form-group">
                <div class="form-label-group">
                    <input name="poster_material" type="text" id="inputPosterMaterial" class="form-control" placeholder="PosterMaterial" required="required">
                    <label for="inputPosterMaterial">Poster Material</label>
                </div>
            </div>


            <div class="form-group">
                <div class="form-label-group">
                    <input name="vision" type="text" id="inputVision" class="form-control" placeholder="Vison" required="required">
                    <label for="inputVision">Vision</label>
                </div>
            </div>


            <div class="form-group">
                <div class="form-label-group">
                    <input name="comment" type="text" id="inputComment" class="form-control" placeholder="Comment" required="required">
                    <label for="inputComment">Comment</label>
                </div>
            </div>




            <button name="submit" class="btn btn-primary btn-block">Submit</button>
          </form>
          
        </div>
      </div>
    </div>




    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

     


  </body>

</html>