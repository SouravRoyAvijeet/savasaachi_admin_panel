<?php

    include "auth/auth_writer.php"

?>

<?php
  date_default_timezone_set('Asia/Dhaka');
?>

<?php  
    require("connection.php"); 
    /*$query = "SELECT * FROM content";  
    $result = mysqli_query($conn, $query); */
?>  

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Fonts-->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Lobster|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
   

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">


    <style type="text/css">
      
      .one_line{
            
            line-height: 5px;
            white-space: nowrap;
            overflow: hidden;
            max-width: 120px; 
        }
    </style>

  </head>

  <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="custom-nav">

          <a class="navbar-brand logo-txt" href="http://localhost/savasaachi_admin_panel/writer.php">Savasaachi</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse form-txt-two" id="navbarSupportedContent">

              <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

                <li class="nav-item ">
                  <a class="nav-link" href="#order_table">Content</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#con">Write Content</a>
                </li>


                  <!-- User Menu-->

                      <li class="dropdown" style="position: absolute;right: 0;top:0"> <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
                          <ul class="dropdown-menu settings-menu dropdown-menu-right">
                              <li><a class="dropdown-item" href="#"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                              <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                              <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                          </ul>
                      </li>

              </ul>
             
          </div>

        </nav>

    <div id="wrapper" >

      <!-- Sidebar -->

      <ul class="sidebar navbar-nav">

         <!-- Search -->
      <input class="form-control" id="myInput" type="text" placeholder="Search..">



        <form action="filter.php" method="POST">


              <div class="row">
            
                      <div class="col-md-5">
                        
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date" value="<?php echo  date("d/m/Y") ?> " />

                      </div> 

                      <div class="col-md-3">
                        
                          <input type="button" name="filter" id="filter" value="Filter" class="btn btn-info data-filter" /> 

                      </div> 

                      <div class="col-md-6">

                      
                       <input  name="name" type="hidden" id="name" class="form-control"  value="" >


                      </div>       

              </div>

        </form>    



        <!-- Get business name -->

        <form action="filter_business.php" method="POST">

               <input type="hidden" id="destination_three" name="name" value="" class="form-control">

               <input type="button" name="filter_bs" id="filter_bs" value="Business" class="btn btn-info data-filter" />
          
             <?php

                  session_start();
                        
                  require("connection.php");

                            
                  $result = mysqli_query($conn, "SELECT * FROM business");
                  
                  while($row=mysqli_fetch_array($result)){

                    $id=$row['id'];
                      
                    
                                    
                    ?>

                    <li id="myTable" class="bs-list"><a href="#" 
                    class='sourcelink btn btn-info' name="name" id="bs-link"> <?php echo $row['name']?></a></li>

                    <?php

                   }

              ?>              

        </form>

        
      </ul>


   <div id="content-wrapper" class="form-txt-five">

        <div class="container-fluid">

          <!--Business Detail -->

                         
            <div class="row">
              <div id="business-table" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                <table class="table table-hover table-bordered d-none">
                  <thead>
                  <tr>  
                    <th>Business Name</th>
                    <th>Address</th>
                    <th>Facebook</th>
                    <th>Phone</th>
                    <th>Web</th>
                    <th>Package</th>  
                    <th>Type</th>
                    <th>Country</th>            
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php         

                  while($row=mysqli_fetch_array($result)){

                    $id=$row['id'];
                    echo "<tr>";
                   
                    echo  "<td>"  .  $row['name']      . "</td>";
                    echo  "<td>"  .  $row['address']     . "</td>";
                    echo  "<td>"  .  $row['facebook']    . "</td>";
                    echo  "<td>"  .  $row['phone']       . "</td>";
                    echo  "<td>"  .  $row['web']         . "</td>";
                    echo  "<td>"  .  $row['package']     . "</td>";
                    echo  "<td>"  .  $row['type']        . "</td>";
                    echo  "<td>"  .  $row['country']     . "</td>";

                    ?>
                    
                    <?php
                    echo "</tr>";
                        }

                  ?>
                  </tbody>
                </table>
            </div>  
          </div>  
    <!--End Business Detail -->    


   <!--Content Form -->
    <div id="con" class="card">
        <div class="card-header col-md-12">Add Content</div>

      <form action="add_content_system.php" method="POST">
        <div class="card-body">

         

          <div class="row">
              
              <div class="col-md-6">
                  
                  <div class="form-group">
                  <div class="form-label-group">                                       

                       <input name="name" type="text"  id="destination_two" class="form-control" placeholder="Name" required="required" autofocus="autofocus" value="" >
                       <label for="destination_two">Name</label> 
                    
                    </div>
                  </div>

              </div>  

               <div class="col-md-6">
                  <div class="form-group">
                   
                    <div class="form-label-group">
                        <input type="text" name="date" id="date" class="form-control" placeholder="From Date" value="<?php echo  date("d/m/Y") ?> ">
                     
                    </div>
                  </div>
              </div>            
                               
          </div>

           

          <div class="row">
            <div class="col-md-6">
             <div class="form-group">           
                <div class="form-label-group">
                    <input name="post_material" type="text" id="inputPostMaterial" class="form-control" placeholder="Post Material" required="required">
                    <label for="inputPostMaterial">Post Material</label>
                </div>
               </div> 
            </div>

            <div class="col-md-6">
             <div class="form-group">
                <div class="form-label-group">
                    <input name="tags" type="text" id="inputTags" class="form-control" placeholder="Tags" required="required">
                    <label for="inputTags">Tags</label>
                </div>
                </div>            
            </div>
          </div>  


          <div class="row">
              <div class="col-md-6">
                 <div class="form-group">          
                  <div class="form-label-group">
                      <input name="poster_material" type="text" id="inputPosterMaterial" class="form-control" placeholder="PosterMaterial" required="required">
                      <label for="inputPosterMaterial">Poster Material</label>
                  </div>
                  </div>
              </div>

             <div class="col-md-6">
                    <div class="form-group">          
                    <div class="form-label-group">
                        <input name="vision" type="text" id="inputVision" class="form-control" placeholder="Vison" required="required">
                        <label for="inputVision">Vision</label>
                    </div>
                    </div>

             </div>

          </div>
            
            <div class="form-group">
                <div class="form-label-group">
                    <textarea name="comment" type="text" id="inputComment" class="form-control" placeholder="Comment" required="required" style="height: 100px;"></textarea>
                    
                </div>
            </div>
       
        </div>

          <div class="card-footer text-right">
            
            <button name="submit" class="btn btn-info btn-sm custom-btn">Submit</button>

          </div>

      </form>

      </div> <!-------End Content Form ------>


<!----------Fetch Content Details ------->                             
<?php         
session_start();                        
require("connection.php");
//   $date = date("d/m/Y");                      
$result = mysqli_query($conn,"SELECT * FROM content order by id desc limit 1");                         
if(mysqli_num_rows($result) > 0){

?>
        
  <div class="card">
      <div class="card-header">
            <div class="heading float-left">
                <h4>Contents</h4>
             </div>
      </div>

      <div class="card-body">
          <div class="table-responsive" id="order_table">
        
           <table class="table table-bordered table-striped table-hover">

        <thead>
          <tr>  
            <th>Business Name</th>
            <th>Date</th>
            <th>Post Material</th>
            <th>Tags</th>
            <th>Poster Material</th>
            <th>Vision</th>  
            <th>Comment</th>
            <th>Edit</th>            
          </tr>
        </thead>
        
<?php

while($row=mysqli_fetch_array($result)){

     $name            =     $row['name']  ;           
     $date            =     $row['date'] ;            
     $post_material   =     $row['post_material'];
     $tags            =     $row['tags']    ;         
     $poster_material =     $row['poster_material']    ;  
     $vision          =     $row['vision']    ;       
     $comment         =     $row['comment'] ;  
     $id = $row['id'];  

?>                                              
<tbody>            
     <tr>                                            
        <td class="one_line"><?php echo $name;?></td>
        <td class="one_line"><?php echo $date;?></td>
        <td class="one_line"><?php echo $post_material;?></td>
        <td class="one_line"><?php echo $tags;?></td>
        <td class="one_line"><?php echo $poster_material;?></td>
        <td class="one_line"><?php echo $vision;?></td>
        <td class="one_line"><?php echo $comment;?></td>
        <td><a href="update.php?id=<?php echo $id;?>" class='btn btn btn-info btn-sm custom-btn' name="name" >Edit</a></td>
    </tr>
<?php 

  }

}
                      
    ?>
    </tbody>
    </table>                  
    </div>
    </div>
    </div>       
    <?php               
                      
?> <!--------End Content Details --------->
             
      <!-- /.container-fluid -->
        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->



    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>






<!-- One date input to another  -->
<script >

    $('input[name="date"]').change(function() {
    $('input[name="date"]').val($(this).val());
    });

</script>
<!-- One date input to another  -->


<!-- Taking input from button with js-->
<script type="text/javascript">
  
    $(document).ready(function() {
    $('.sourcelink').click(function() {
    $('#destination_two').val($(this).html());
    $('#destination_three').val($(this).html());
    $('#name').val($(this).html());

    });
    });

</script>
<!-- // Taking input from button with js-->


<!-- fetching data using date and business name with ajax -->
    <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'mm/dd/yy'   
           });  
           $(function(){  
                $("#date").datepicker();
                $("#name");   
           });  
           $('#filter').click(function(){  
                var date = $('#date').val();
                var name = $('#name').val();   
                if(date != '' && name != '')  
                {  
                     $.ajax({  
                          url:"filter_writer_content.php",  
                          method:"POST",  
                          data:{date:date, name:name},  
                          success:function(data)  
                          {  
                              $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Business");  
                }  
           });  
      });  
    </script>
<!-- fetching data using date and business name with ajax-->


<!-- fetching data using date and business name with ajax -->
    <script>  
      $(document).ready(function(){  

           $(function(){  
                $("#destination_three");   
           });  
           $('#filter_bs').click(function(){  
                var name = $('#destination_three').val();   
                if(name != '')  
                {  
                     $.ajax({  
                          url:"filter_writer_business.php",  
                          method:"POST",  
                          data:{name:name},  
                          success:function(data)  
                          {  
                              $('#business-table').html(data);  
                          }  
                     });  
                }

                else  
                {  
                     alert("Please Select Business");  
                }    
           });  
      });  
    </script>
<!-- fetching data using date and business name with ajax-->


<!-- date picker -->

     <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'dd/mm/yy'   
           });  
           $(function(){  
                $("#date").datepicker();  
           });  
             
      });  
    </script>

<!-- date picker -->


<!--Current Date in js for 1st date input-->

        <script>

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            } 

            if(mm<10) {
                mm = '0'+mm
            } 

            today = dd + '/' + mm + '/' + yyyy;

        </script>

 <!--Current Date in js -->





     <!-- Search -->

      <script>
                $(document).ready(function(){
                  $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable a").filter(function() {
                      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                  });
                });

      </script> 

    <!-- Search -->



    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>

     


  </body>

</html>
