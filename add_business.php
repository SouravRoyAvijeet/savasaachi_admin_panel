<?php

    include "auth/auth_admin.php"

?>

<?php
  date_default_timezone_set('Asia/Dhaka');

?>

<?php  
    require("connection.php"); 
?>  

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Fonts-->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Lobster|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
   

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">

  </head>

<body id="page-top">


    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="custom-nav">

        <a class="navbar-brand logo-txt" href="http://localhost/savasaachi_admin_panel/dashboard.php">Savasaachi</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

            </ul>

        </div>

    </nav>


    <!--Business Form -->

            <div class="container form-txt-five">
                <div id="con" class="card">
                    <div class="card-header col-md-12">
                      Add Business
                      <a href="javascript:history.back()" class="btn btn-info btn-sm float-right custom-btn">Back</a>
                    </div>

                  <form action="add_business_system.php" method="POST">
                    <div class="card-body">
                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <div class="form-label-group">

                                             <input name="name" type="text" id="firstName" class="form-control" placeholder="First name" required="required" autofocus="autofocus">

                                            <label for="firstName">Name</label>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">

                                        <div class="form-label-group">
                                            
                                            <input name="address" type="text" id="inputAddress" class="form-control" placeholder="Address" required="required" autofocus="autofocus">
                                            <label for="inputAddress">Address</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                             <input name="facebook" type="text" id="inputFacebook" class="form-control" placeholder="facebook" required="required">
                                            <label for="inputFacebook">Facebook</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            
                                            <input name="phone" type="text" id="inputPhone" class="form-control" autofocus="autofocus"  placeholder="phone" required="required">

                                            <label for="inputPhone">Phone</label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input name="web" type="text" id="inputWeb" class="form-control" placeholder="website" required="required">
                                            <label for="inputWeb">Web</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input name="package" type="text" id="inputPackage" class="form-control" placeholder="Package" required="required">
                                            <label for="inputPackage">Package</label>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                    <div class="form-label-group">
                                       <input name="type" type="text" id="inputType" class="form-control" placeholder="Type" required="required">
                                        <label for="inputType">Type</label>
                                    </div>
                                  </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="form-label-group">
                                      <input name="country" type="text" id="inputCountry" class="form-control" placeholder="facebook" required="required">
                                      <label for="inputCountry">country</label>
                                  </div>
                                </div>
                              </div>

                        <?php         
                            session_start();                                                      
                            require("connection.php");                                              
                                $designer = "designer";
                                $sql="SELECT * FROM user WHERE role= '$designer' ";
                                $result = mysqli_query($conn, $sql);
                                       if(mysqli_num_rows($result) > 0){
                                      
                        ?>
                            <div class="col-md-12">
                                <select class="form-control" name="designer">    
                                <option value="0" selected="true" disabled="true">Designer:</option>        
                                    <?php while($row=mysqli_fetch_array($result)):;?>
                                    <option><?php echo $row['name'];?></option>
                                    <?php endwhile;?>
                                </select>
                            </div>
                        <?php 
                           
                           }                    
                       ?>
                        </div>

                     </div>

                    <div class="card-footer text-right">
                      <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:history.back()" class="btn btn-info btn-sm custom-btn">Cancle</a>
                            <button type="submit" class="btn btn-info btn-sm custom-btn" name="submit">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>


            <!-- Sticky Footer -->
            <footer class="sticky-footer-two">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © Your Website 2018</span>
                    </div>
                </div>
            </footer>




    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>

     


  </body>

</html>
