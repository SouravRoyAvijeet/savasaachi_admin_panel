<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
   

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">


    <style type="text/css">
      
      .one_line{
            
            line-height: 5px;
            white-space: nowrap;
            overflow: hidden;
            max-width: 120px; 
        }
    </style>
  </head>


<!-- PopUP -->
<?php  
 if(isset($_POST["id"]))  
 {  
      $output = '';  
      $connect = mysqli_connect("localhost", "root", "", "savasaachi_admin_panel");  
      $query = "SELECT * FROM content WHERE id = '".$_POST["id"]."'";  
      $result = mysqli_query($connect, $query);  
      $output .= '  
      <div class="table-responsive">  
           <table class="table table-bordered">';  
      while($row = mysqli_fetch_array($result))  
      {  
           $output .= '  
                <tr>  
                     <td width="30%"><label>Name</label></td>  
                     <td width="70%">'.$row["name"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>Date</label></td>  
                     <td width="70%">'.$row["date"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>Post Material</label></td>  
                     <td width="70%">'.$row["post_material"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>Tags</label></td>  
                     <td width="70%">'.$row["tags"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>Poster Material</label></td>  
                     <td width="70%">'.$row["poster_material"].' </td>  
                </tr>
                 <tr>  
                     <td width="30%"><label>Vision</label></td>  
                     <td width="70%">'.$row["vision"].' </td>   
                </tr>
                 <tr>  
                     <td width="30%"><label>Comment</label></td>  
                     <td width="70%">'.$row["comment"].'</td>  
                </tr>      
           ';  
      }  
      $output .= '  
           </table>  
      </div>  
      ';  
      echo $output;  
 }  
 ?>
<!-- PopUP -->

     <?php         
                
            require("connection.php");

            if(isset($_POST["name"]))

            {  
              echo '<table class="table table-bordered table-striped table-hover">';
              echo '<thead>';

              echo '<tr>  
              
                        <th>Business Name</th>
                        <th>Date</th>
                        <th>Post Material</th>
                        <th>Tags</th>
                        <th>Poster Material</th>
                        <th>Vision</th>  
                        <th>Comment</th>
                        <th>View</th>
                              
              </tr>';

              echo '</thead>' ; 
              echo '<tbody>';

                     $date =  trim($_POST["date"]);
                     $name =  trim($_POST["name"]);

$result = mysqli_query($conn, "SELECT *FROM content
WHERE date= '".$date."' AND name= '".$name."' ");

                  
                    
                      if(mysqli_num_rows($result) > 0){

                          while($row=mysqli_fetch_array($result)){

                            //$id=$row['id'];
                            echo "<tr>";
                           
                            echo  "<td class='one_line'>"  .  $row['name']                    . "</td>";
                            echo  "<td class='one_line'>"  .  $row['date']                    . "</td>";
                            echo  "<td class='one_line'>"  .  $row['post_material']           . "</td>";
                            echo  "<td class='one_line'>"  .  $row['tags']                    . "</td>";
                            echo  "<td class='one_line'>"  .  $row['poster_material']         . "</td>";
                            echo  "<td class='one_line'>"  .  $row['vision']                  . "</td>";
                            echo  "<td class='one_line'>"  .  $row['comment']                 . "</td>";

                           ?>
                              <td><input type="button" name="view" value="view" id="<?php echo $row["id"]; ?>" class="btn btn-info btn-sm view_data custom-btn" /></td>
                           <?php   
                            
                            echo "</tr>";
                          }

                     }
                  }

             echo '</tbody>';
             echo '</table>';
        ?>



<!-- Model -->
 <div id="dataModal" class="modal fade">  
      <div class="modal-dialog modal-lg">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <h4 class="modal-title t" >Content Details</h4>  
                </div>  
                <div class="modal-body" id="content_detail">  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div> 
<!-- Model -->






<!-- Model View -->
 <script>  
 $(document).ready(function(){  
  
      $(document).on('click', '.view_data', function(){  
           var id = $(this).attr("id");  
           if(id != '')  
           {  
                $.ajax({  
                     url:"filter_content_designer.php",  
                     method:"POST",  
                     data:{id:id},  
                     success:function(data){  
                          $('#content_detail').html(data);  
                          $('#dataModal').modal('show');  
                     }  
                });  
           }            
      });  
 });  
 </script>
 <!-- Modal View -->



    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
     


  </body>

</html>





