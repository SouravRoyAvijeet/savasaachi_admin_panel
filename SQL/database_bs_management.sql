-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2018 at 11:41 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `savasaachi_admin_panel`
--

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `facebook` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `web` varchar(30) NOT NULL,
  `package` varchar(30) NOT NULL,
  `type` varchar(20) NOT NULL,
  `country` varchar(30) NOT NULL,
  `designer` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `name`, `address`, `facebook`, `phone`, `web`, `package`, `type`, `country`, `designer`) VALUES
(37, 'XYZ', 'sylhet', 'facebook.com', '01888888', 'arman.com', 'nan', 'nan', 'bangladesh', 'nikhil'),
(38, 'ABC', 'sylhet', 'bluemile.com', '123', 'bluemile.com', 'nan', 'nan', 'bd', 'Subarna'),
(39, 'JHA', 'Dhaka', 'facebook.com', '01999999', 'Cardamon.com', 'Gold', 'Restaurant', 'bd', 'nikhil'),
(40, 'TEA', 'Dhaka', 'facebook.com', '029965', 'eee', 'Gold', 'Restaurant', 'Bangladesh', 'Subarna');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `date` varchar(30) NOT NULL,
  `post_material` varchar(700) NOT NULL,
  `tags` varchar(50) NOT NULL,
  `poster_material` varchar(700) NOT NULL,
  `vision` varchar(700) NOT NULL,
  `comment` varchar(700) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `name`, `date`, `post_material`, `tags`, `poster_material`, `vision`, `comment`) VALUES
(151, 'XYZ', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...'),
(152, 'ABC', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(153, 'XYZ', '24/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(154, 'XYZ', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(155, 'XYZ', '24/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(156, 'XYZ', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(157, 'XYZ', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(158, 'ABC', '24/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(159, 'ABC', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...'),
(160, 'ABC', '26/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'lLorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(161, 'ABC', '26/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'lLorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(162, 'ABC', '26/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'lLorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(163, 'ABC', '26/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'lLorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(164, 'ABC', '26/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'lLorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(179, 'JHA', '27/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(180, 'JHA', '27/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(181, 'JHA', '27/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(182, 'JHA', '27/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...Lorem Ipsum is simply dummy text of the printing a...'),
(183, 'JHA', '28/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(184, 'JHA', '28/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(185, 'JHA', '27/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a...', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(186, 'JHA', '28/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(187, 'JHA', '27/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum. Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(188, 'JHA', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(189, 'JHA', '29/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(190, 'ABC', '29/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(191, 'ABC', '25/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(192, 'ABC', '28/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(193, 'ABC', '29/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(194, 'JHA', '29/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(195, 'JHA', '16/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(196, 'JHA', '29/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(197, 'JHA', '29/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.'),
(198, 'JHA', '10/12/2018', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing a', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(20) NOT NULL,
  `role` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `role`, `password`) VALUES
(33, 'Avijeet', 'avi@gmail.com', 'admin', '81dc9bdb52d04dc20036dbd8313ed055'),
(34, 'Sourav', 'sourav@gmail.com', 'writer', '81dc9bdb52d04dc20036dbd8313ed055'),
(35, 'Subarna', 'subarna@gmail.com', 'designer', '81dc9bdb52d04dc20036dbd8313ed055'),
(36, 'Anik', 'an@gmail.com', 'designer', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `name_2` (`name`),
  ADD UNIQUE KEY `name_3` (`name`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
