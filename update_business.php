<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Fonts-->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Lobster|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">


    <style rel="stylesheet" type="text/css" href="style.css">
  
    #img_div{
    width: 80%;
    padding: 5px;
    margin: 15px auto;
    border: 1px solid #cbcbcb;
     }
     #img_div:after{
    content: "";
    display: block;
    clear: both;
     }
     img{
    float: left;
    margin: 5px;
    width: 200px;
    height: 140px;
     }

    #bs-link {
    width: 100%;
    margin-top: 11px;
    }

    input#name.form-control {
      padding-left: 0px;
    }
    
    input#destination_two.form-control{
      padding-left: 0px;
    }

 </style>

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-light bg-light" id="custom-nav">

        <a class="navbar-brand logo-txt" href="http://localhost/savasaachi_admin_panel/dashboard.php">Savasaachi</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

            </ul>

        </div>

    </nav>


<?php
session_start();
require("connection.php");
$name=$_GET['name'];

if(isset($name))
{


  $readdata= "SELECT * FROM business WHERE name = '$name' "; 
  $queries= mysqli_query($conn,$readdata);
  $queriesarray=mysqli_fetch_array($queries);
  $name=$queriesarray['name'];
  $address=$queriesarray['address'];
  $facebook=$queriesarray['facebook'];
  $phone=$queriesarray['phone'];
  $web=$queriesarray['web'];
  $package=$queriesarray['package'];
  $type=$queriesarray['type'];
  $country=$queriesarray['country'];
                
}


if (isset($_POST['updatedata'])) {

  $name=$_POST['name'];
  $address=$_POST['address'];
  $facebook=$_POST['facebook'];
  $phone=$_POST['phone'];
  $web=$_POST['web'];
  $package=$_POST['package'];
  $type=$_POST['type'];
  $country=$_POST['country'];

  $upquery="UPDATE `business` SET  `name`='$name',`address`='$address',`facebook`='$facebook',`phone`='$phone',
  `web`='$web',`package`='$package', `type`='$type',`country`='$country' WHERE name=  '".$name."'"  ;
  $updated=mysqli_query($conn,$upquery);
  if($updated==true)
  {
    ?>
    <script>alert("Record updated Successfully");
    window.location.href="dashboard.php";</script>
    <?php
  }
}
?>



            <!--Content Form -->

            <div class="container form-txt-five">
                <div id="con" class="card">
                    <div class="card-header col-md-12">
                      Update Business
                      <a href="javascript:history.back()" class="btn btn-info btn-sm float-right custom-btn">Back</a>
                    </div>

                  <form action="#" method="POST" enctype="multipart/form-data">
                    <div class="card-body">
                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <div class="form-label-group">

                                            <input type="text" class="form-control" id="name" placeholder="name" name="name" value="<?php echo $name; ?>">
                                            <label for="name">Name</label>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">

                                        <div class="form-label-group">
                                            <input type="text" name="address" id="address" class="form-control" placeholder="Address" value="<?php echo $address; ?>" style="margin-top: 10px;">
                                            <label for="address">Address</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                             <input type="text" class="form-control" id="facebook" placeholder="facebook" name="facebook" value="<?php echo $facebook; ?>">
                                            <label for="facebook">Facebook</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" id="phone" placeholder="Enter division" name="phone" value="<?php echo $phone; ?>">
                                            <label for="phone">Phone</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" id="web" placeholder="web" name="web" value="<?php echo $web; ?>">
                                            <label for="web">Web</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <input class="form-control" rows="5" id="package" name="package" value="<?php echo $package; ?>">
                                            <label for="package">Package</label>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                    <div class="form-label-group">
                                        <input class="form-control" rows="5" id="type" name="type" value="<?php echo $type; ?>">
                                        <label for="type">Type</label>
                                    </div>
                                  </div>
                              </div>

                              <div class="col-md-6">
                                <div class="form-group">
                                  <div class="form-label-group">
                                      <input class="form-control" rows="5" id="country" name="country" value="<?php echo $country; ?>">
                                      <label for="country">country</label>
                                  </div>
                                </div>
                              </div>
                          </div>

                     </div>

                    <div class="card-footer text-right">
                      <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="javascript:history.back()" class="btn btn-info btn-sm custom-btn">Cancle</a>
                            <button type="submit" class="btn btn-info btn-sm custom-btn" name="updatedata">Submit</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>



            <!-- Sticky Footer -->
            <footer class="sticky-footer-two form-txt-two">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © Your Website 2018</span>
                    </div>
                </div>
            </footer>


   <!-- Datepicker -->


     <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'dd/mm/yy'   
           });  
           $(function(){  
                $("#date2").datepicker();  
           });  
             
      });  
    </script>


     <!-- Search -->

     <script>
                $(document).ready(function(){
                  $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable a").filter(function() {
                      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                  });
                });

      </script> 




    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

     


  </body>

</html>