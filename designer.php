<?php
    include "auth/auth_designer.php"
?>

<?php 
 $designer_name =  $_SESSION["name"];
?>


<?php
  date_default_timezone_set('Asia/Dhaka');
?>

<?php  
    require("connection.php"); 
    /*$query = "SELECT * FROM content";  
    $result = mysqli_query($conn, $query); */
?>  

<!DOCTYPE html>
<html lang="en">

  <head>

 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Fonts-->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://fonts.googleapis.com/css?family=Charm" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charm|Lobster|Staatliches" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">


    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">

    <style type="text/css">
      
      .one_line{
            
            line-height: 5px;
            white-space: nowrap;
            overflow: hidden;
            max-width: 120px; 
        }
    </style>


  </head>

  <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-light bg-light" id="custom-nav">

          <a class="navbar-brand logo-txt" href="http://localhost/savasaachi_admin_panel/designer.php">Savasaachi</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse form-txt-two" id="navbarSupportedContent">

              <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

                <li class="nav-item ">
                  <a class="nav-link" href="#order_table">Content</a>
                </li>


                  <!-- User Menu-->

                      <li class="dropdown" style="position: absolute;right: 0;top:0"> <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
                          <ul class="dropdown-menu settings-menu dropdown-menu-right">
                              <li><a class="dropdown-item" href="#"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
                              <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> <?php echo $designer_name; ?></a></li>
                              <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                          </ul>
                      </li>

              </ul>
             
          </div>

        </nav>

    <div id="wrapper">

      <!-- Sidebar -->

      <ul class="sidebar navbar-nav">

         <!-- Search -->
      <input class="form-control" id="myInput" type="text" placeholder="Search..">



        <form action="filter.php" method="POST">


              <div class="row">
            
                      <div class="col-md-5">
                        
                          <input type="text" name="date" id="date" class="form-control" placeholder="Date" value="<?php echo  date("d/m/Y") ?> " />

                      </div> 

                      <div class="col-md-3">
                        
                          <input type="button" name="filter" id="filter" value="Filter" class="btn btn-info data-filter" /> 

                      </div> 

                      <div class="col-md-6">

                       <input  name="name" type="hidden" id="name" class="form-control"  value="<?php echo  
                       $designer_name ?> " >

                      </div>       

              </div>

        </form>    



        <!-- Get business name -->

        <form action="filter_business.php" method="POST">

               <input type="hidden" id="destination_three" name="name" value="" class="form-control">
               
               <input type="button" name="filter_bs" id="filter_bs" value="Business" class="btn btn-info data-filter" />
          
             <?php

                  session_start();
                        
                  require("connection.php");

                            
                  $result = mysqli_query($conn, "SELECT * FROM business WHERE designer =  '".$designer_name."' ");
                  
                  while($row=mysqli_fetch_array($result)){

                    $id=$row['id'];
                      
                    
                                    
                    ?>

                    <li id="myTable" class="bs-list"><a href="#" id="bs-link"
                    class='sourcelink btn btn-info' name="name"> <?php echo $row['name']?></a></li>

                    <?php

                   }

              ?>              

        </form>
     
      </ul>


   <div id="content-wrapper" class="form-txt-five">

        <div class="container-fluid">

          <!--Business Detail -->

                         
            <div class="row">
              <div id="business-table" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                <table class="table table-hover table-bordered d-none">
                  <thead>
                  <tr>  
                    <th>Business Name</th>
                    <th>Address</th>
                    <th>Facebook</th>
                    <th>Phone</th>
                    <th>Web</th>
                    <th>Package</th>  
                    <th>Type</th>
                    <th>Country</th>            
                  </tr>
                  </thead>
                  <tbody>
                  
                  <?php         

                  while($row=mysqli_fetch_array($result)){

                    $id=$row['id'];
                    echo "<tr>";
                   
                    echo  "<td>"  .  $row['name']      . "</td>";
                    echo  "<td>"  .  $row['address']     . "</td>";
                    echo  "<td>"  .  $row['facebook']    . "</td>";
                    echo  "<td>"  .  $row['phone']       . "</td>";
                    echo  "<td>"  .  $row['web']         . "</td>";
                    echo  "<td>"  .  $row['package']     . "</td>";
                    echo  "<td>"  .  $row['type']        . "</td>";
                    echo  "<td>"  .  $row['country']     . "</td>";

                    ?>
                    
                    <?php
                    echo "</tr>";
                        }

                  ?>
                  </tbody>
                </table>
            </div>  
          </div>  
    <!--End Business Detail -->    

      <!--Fetch Content Details In Term Of Date And Business Name-->
                 
               <div class="card">
                  <div class="card-header">
                        <div class="heading float-left">
                            <h4>Contents</h4>
                         </div>
                  </div>

                  <div class="card-body">

                  <div id="order_table" style="width: 100%;">  
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>  
                              <th>Business Name</th>
                              <th>Date</th>
                              <th>Post Material</th>
                              <th>Tags</th>
                              <th>Poster Material</th>
                              <th>Vision</th> 
                              <th>Comment</th>  
                              <th>View</th>                                          
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php         
                            session_start();                        
                                
                            require("connection.php");
                                                 
                            $date = date("d/m/Y");
                            /*$result = mysqli_query($conn, "SELECT * FROM content WHERE date =  '".$date."' AND ");*/

                            $result = mysqli_query($conn, "SELECT *FROM business
                  INNER JOIN content ON content.name=business.name
                  WHERE date= '".$date."'  AND business.designer = '".$designer_name."' ");
                                                        

                                          if(mysqli_num_rows($result) > 0){

                                          while($row=mysqli_fetch_array($result)){

                                                    $id              =     $row['id'];
                                                    $name            =     $row['name']  ;           
                                                    $date            =     $row['date'] ;            
                                                    $post_material   =     $row['post_material'];
                                                    $tags            =     $row['tags']    ;         
                                                    $poster_material =     $row['poster_material']    ;  
                                                    $vision          =     $row['vision']    ;       
                                                    $comment         =     $row['comment'] ;    

                                                    ?>

                                                      <tr>                                            
                                                         <td class="one_line"><?php echo $name;?></td>
                                                         <td class="one_line"><?php echo $date;?></td>
                                                         <td class="one_line"><?php echo $post_material;?></td>
                                                         <td class="one_line"><?php echo $tags;?></td>
                                                         <td class="one_line"><?php echo $poster_material;?></td>
                                                         <td class="one_line"><?php echo $vision;?></td>
                                                         <td class="one_line"><?php echo $comment;?></td>

                                                      <td><input type="button" name="view" value="view" id="<?php echo $row["id"]; ?>" class="btn btn-info btn-sm view_data custom-btn" /></td>
                                                         
                                                      </tr>

                                                    <?php 



                                          }

                                       }
                                                                

                                ?>

                            </tbody>
                          </table>
                       </div>
                  </div>
               </div>




<!-- Model -->
 <div id="dataModal" class="modal fade">  
      <div class="modal-dialog modal-lg">  
           <div class="modal-content">  
                <div class="modal-header">  
                     
                     <h4 class="modal-title t" >Content Details</h4>  
                </div>  
                <div class="modal-body" id="content_detail">  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div> 
<!-- Model -->


<!--End Content Details -->
             
<!-- /.container-fluid -->



        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->






<!-- Model View -->
<script>  
 $(document).ready(function(){  
  
      $(document).on('click', '.view_data', function(){  
           var id = $(this).attr("id");  
           if(id != '')  
           {  
                $.ajax({  
                     url:"select_modal.php",  
                     method:"POST",  
                     data:{id:id},  
                     success:function(data){  
                          $('#content_detail').html(data);  
                          $('#dataModal').modal('show');  
                     }  
                });  
           }            
      });  
 });  
 </script>

 

 
 <!-- Modal View -->



<!-- One date input to another  -->
<script >

    $('input[name="date"]').change(function() {
    $('input[name="date"]').val($(this).val());
    });

</script>
<!-- One date input to another  -->


<!-- Taking input from button with js-->
<script type="text/javascript">
  
    $(document).ready(function() {
    $('.sourcelink').click(function() {
    $('#destination_three').val($(this).html());
    $('#bs_name').val($(this).html());

    });
    });

</script>

<!-- // Taking input from button with js-->


<!-- fetching data using date  with ajax -->
    <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'mm/dd/yy'   
           });  
           $(function(){  
                $("#date").datepicker();
                $("#name");   
               
           });  
           $('#filter').click(function(){  
                var date = $('#date').val();
                var name = $('#name').val();   
                if(date != '' && name != '')   
                {  
                     $.ajax({  
                          url:"filter_content_designer.php",  
                          method:"POST",  
                          data:{date:date, name:name},    
                          success:function(data)  
                          {  
                              $('#order_table').html(data);  
                          }  
                     });  
                }  
                else  
                {  
                     alert("Please Select Business");  
                }  
           });  
      });  
    </script>
<!-- fetching data using date and business name with ajax-->


<!-- fetching business details using business name with ajax -->
    <script>  
      $(document).ready(function(){  

           $(function(){  
                $("#destination_three");   
           });  
           $('#filter_bs').click(function(){  
                var name = $('#destination_three').val();   
                if(name != '')  
                {  
                     $.ajax({  
                          url:"filter_writer_business.php",  
                          method:"POST",  
                          data:{name:name},  
                          success:function(data)  
                          {  
                              $('#business-table').html(data);  
                          }  
                     });  
                }

                else  
                {  
                     alert("Please Select Business");  
                }    
           });  
      });  
    </script>
<!-- fetching business details using date and business name with ajax-->


<!-- fetching content using business name with ajax -->
<script>  
      $(document).ready(function(){  

         $.datepicker.setDefaults({  
                dateFormat: 'mm/dd/yy'   
           }); 

           $(function(){  
                $("#date").datepicker();
                $("#destination_three");   
           });  
           $('.sourcelink').click(function(){  
                var date = $('#date').val();
                var name = $('#destination_three').val();  
                 
                if( date != '' && name != '')  
                {  
                     $.ajax({  
                          url:"filter_designer_content.php",  
                          method:"POST",  
                          data:{date:date, name:name},  
                          success:function(data)  
                          {  
                              $('#order_table').html(data);  
                          }  
                     });  
                }

                else  
                {  
                     alert("Please Select Business");  
                }    
           });  
      });  
    </script>
<!-- fetching data using date and business name with ajax-->


<!-- date picker -->

     <script>  
      $(document).ready(function(){  
           $.datepicker.setDefaults({  
                dateFormat: 'dd/mm/yy'   
           });  
           $(function(){  
                $("#date").datepicker();  
           });  
             
      });  
    </script>

<!-- date picker -->


<!--Current Date in js for 1st date input-->

        <script>

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            } 

            if(mm<10) {
                mm = '0'+mm
            } 

            today = dd + '/' + mm + '/' + yyyy;

        </script>

 <!--Current Date in js -->





     <!-- Search -->

      <script>
                $(document).ready(function(){
                  $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable a").filter(function() {
                      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                  });
                });

      </script> 

    <!-- Search -->



    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>

     


  </body>

</html>
