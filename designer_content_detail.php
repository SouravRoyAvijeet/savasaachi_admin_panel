<?php

    include "auth/auth_designer.php"

?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/style.css" rel="stylesheet">

  </head>


    <body id="page-top">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">

          <a class="navbar-brand" href="http://localhost/savasaachi_admin_panel/designer.php">Savasaachi</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">

              <ul class="navbar-nav mr-auto nav-list list-inline mx-auto justify-content-center">

                <li class="nav-item ">
                  <a class="nav-link" href="designer_content_detail.php">Content</a>
                </li>

    
              </ul>

              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
              </form>
             
          </div>

        </nav>

<style rel="stylesheet" type="text/css" href="style.css">
	
		#img_div{
		width: 80%;
		padding: 5px;
		margin: 15px auto;
		border: 1px solid #cbcbcb;
	   }
	   #img_div:after{
		content: "";
		display: block;
		clear: both;
	   }
	   img{
		float: left;
		margin: 5px;
		width: 200px;
		height: 140px;
	   }

 </style>
	

	
	<div class="row business-detail-row">
		<div class="col-lg-offset-6 col-md-offset-6 col-lg-4 col-md-4">
			 <h5>Content Details</h5>
		</div>
	   
		<!-- <div class="col-md-4 col-md-offset-8">
			<a href="logout.php" class="btn btn-info logout-btn" >logout</a>
		</div> -->
	</div>
	
	
  <div class ="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			        
				<table class="table table-hover">
				  <thead>
					<tr>	
					  <th>Content Name</th>
					  <th>Date</th>
					  <th>Post Material</th>
					  <th>Tags</th>
					  <th>Poster Material</th>
					  <th>Vision</th>	
					  <th>Comment</th>						
					</tr>
				  </thead>
				  <tbody>
				  
					<?php					
					session_start();
								
							
					require("connection.php");
				
					$business_name = $_GET['name'];	
					$result = mysqli_query($conn,"SELECT * FROM content");
				
					
					while($row=mysqli_fetch_array($result)){

					  $id=$row['id'];
					  echo "<tr>";
					 
					  echo  "<td>"  .  $row['name']  	                . "</td>";
					  echo  "<td>"  .  $row['date']    	        . "</td>";
					  echo  "<td>"  .  $row['post_material']            . "</td>";
					  echo  "<td>"  .  $row['tags']                     . "</td>";
					  echo  "<td>"  .  $row['poster_material']          . "</td>";
					  echo  "<td>"  .  $row['vision']                   . "</td>";
					  echo  "<td>"  .  $row['comment']                  . "</td>";					  

					  echo "</tr>";
					}
					?>
				  </tbody>
				</table>
				</div>	
			</div>	
		</div>


    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>