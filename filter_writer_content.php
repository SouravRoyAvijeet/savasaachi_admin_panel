

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
   

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">

    <style type="text/css">
      
      .one_line{
            
            line-height: 5px;
            white-space: nowrap;
            overflow: hidden;
            max-width: 120px; 
        }
    </style>

  </head>


<?php
require("connection.php");
?>


        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>  
                        <th>Business Name</th>
                        <th>Date</th>
                        <th>Post Material</th>
                        <th>Tags</th>
                        <th>Poster Material</th>
                        <th>Vision</th>  
                        <th>Comment</th>
                        <th>Edit</th>            
                      </tr>
                    </thead>
                  <tbody>
                  
                  <?php         
                  session_start();
                        
                      
                  require("connection.php");

                  if(isset($_POST["date"]))

                  {  
                
                     $date =  trim($_POST["date"]);
                     $name =  trim($_POST["name"]); 
                    
                     $result = mysqli_query($conn,"SELECT * FROM content WHERE date= '".$date."' AND name=  '".$name."' ");
                  
                    
                      if(mysqli_num_rows($result) > 0){

                          while($row=mysqli_fetch_array($result)){

                            $id=$row['id'];
                            echo "<tr>";
                           
                            echo  "<td class='one_line'>"  .  $row['name']                    . "</td>";
                            echo  "<td class='one_line'>"  .  $row['date']                    . "</td>";
                            echo  "<td class='one_line'>"  .  $row['post_material']           . "</td>";
                            echo  "<td class='one_line'>"  .  $row['tags']                    . "</td>";
                            echo  "<td class='one_line'>"  .  $row['poster_material']         . "</td>";
                            echo  "<td class='one_line'>"  .  $row['vision']                  . "</td>";
                            echo  "<td class='one_line'>"  .  $row['comment']                 . "</td>";
            
                            ?>
                              <td><a href="update.php?id=<?php echo $row['id'];?>" class='btn btn btn-info btn-sm custom-btn' name="name" >Edit</a></td>

                            <?php
                            echo "</tr>";
                          }

                     }
                  }
                  ?>

                         </tbody>
                    </table>
                </div>




        







    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
  


  </body>

</html>
