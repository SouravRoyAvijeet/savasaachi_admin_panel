<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Savasaachi Admin - Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
   

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>

    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />



    <!-- Date Format -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>  
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/admin-css/css/main.css">
  


    <link href="css/style.css" rel="stylesheet">

  </head>

  <body class="bg-dark">

    <div class="container" style="margin-top: 150px">



         <!--Content Form -->
    <div id="con" class="card">
          <div class="card-header col-md-12">
                      Register User
              <a href="javascript:history.back()" class="btn btn-info btn-sm float-right">Back</a>
          </div>



      <form action="registration_system.php" method="POST">
        <div class="card-body">
       
          <div class="row">
              
              <div class="col-md-12">
                  
                  <div class="form-group">
                  <div class="form-label-group">                                       

                       <input name="name" type="text" id="firstName" class="form-control" placeholder="Name" required="required" autofocus="autofocus">
                       <label for="firstName">Name</label> 
                    
                    </div>
                  </div>

              </div>

           </div>  
              
          <div class="row">

               <div class="col-md-12">
                  <div class="form-group">
                   
                    <div class="form-label-group">
                        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required">
                        <label for="inputEmail">Email</label> 
                     
                    </div>
                  </div>
              </div>            
                               
          </div>

           

          <div class="row">
            <div class="col-md-12">
             <div class="form-group">           
                <div class="form-label-group">

                <select name="role" class="custom-select" id="inputGroupSelect02">
                    <option selected>Choose Role...</option>
                    <option value="admin">admin</option>
                    <option value="designer">designer</option>
                    <option value="writer">writer</option>
                </select>

                </div>
               </div> 
             </div>
           </div>

           <div class="row">           
              <div class="col-md-12">
               <div class="form-group">
                  <div class="form-label-group">
                      <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
                      <label for="inputPassword">Password</label>
                  </div>
                  </div>            
              </div>
            </div>
 

          <div class="card-footer text-right">
            
            <button name="signup" class="btn btn-info btn-sm" href="login.php" type="submit">Register</button>

              <div class="text-center">
                <a class="d-block small mt-3" href="index.php">Login Page</a>               
              </div>

          </div>

      </form>

      </div>
    </div>


    <!-- Bootstrap core JavaScript-->

    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>

     


  </body>

</html>
